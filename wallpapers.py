#!/usr/bin/env python2
# -*- coding: utf8 -*-

from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import io
import json
import random
import tempfile
import subprocess

import pathlib
import argparse
import requests
import dataset

import StringIO as stringio

from PIL import Image

SOURCES = [
	"http://www.reddit.com/r/earthporn/.json",
	"http://www.reddit.com/r/LakePorn/.json",
	"http://www.reddit.com/r/riverporn/.json",
	"http://www.reddit.com/r/ExposurePorn/.json",
	"http://www.reddit.com/r/wallpaper/top/.json"
	"http://www.reddit.com/r/wallpapers/top/.json"
]

MAX_ASPECT_RATIO = 2560 / 1200
MIN_ASPECT_RATIO = 1920 / 1200

MIN_WIDTH = 1920


def is_image_url(url):
	"""
	Checks if the given *url* points to an image by checking its file extension.
	If it ends with one of the known image-extensions, this method will
	return *True*.

	:type url: unicode
	:param url: The url to check
	:rtype: bool
	"""
	image_extensions = ("jpg", "jpeg", "png")
	return any(url.lower().endswith("." + ext) for ext in image_extensions)


def load_image_urls(source):
	"""
	Loads the given source url, which must point to the json-representation
	of a subreddit. A valid example would be: ``http://www.reddit.com/r/earthporn/.json``.

	:type source: unicode
	:param source: The URL to read from.
	"""
	#: :type: requests.Response
	response = requests.get(source)
	response.raise_for_status()
	data = json.loads(response.content)

	# now get a list of all posts
	#: :type: list
	posts = data.get("data", {}).get("children", ())
	for post in posts:
		if post.get("data", {}).get("over_18"):
			continue

		url = post.get("data", {}).get("url")
		if is_image_url(url):
			yield url


def is_valid_image_size(width, height):
	"""
	Returns *True* if the given *width* and *height* are a good and valid
	image size. An image-size is valid, if its aspect-ratio lays between the
	defined maximum and minimum and if its width exceeds a minimum width.

	:type width: int
	:param width: The width of the image
	:type height: int
	:param height: The height of the image
	:rtype: bool
	"""
	ratio = width / height
	valid = (MIN_ASPECT_RATIO <= ratio <= MAX_ASPECT_RATIO)
	valid = valid and (width >= MIN_WIDTH)
	return valid


def download_image(image_url):
	"""
	Loads the image and returns it as a byte-string.

	:type image_url: unicode
	:param image_url: The url of the image to lod
	:rtype: str
	"""
	#: :type: requests.Response
	response = requests.get(image_url)
	response.raise_for_status()
	return response.content


def parse_arguments():
	"""
	Parses the commandline arguments using argparse and returns the
	argument-object.
	"""
	parser = argparse.ArgumentParser(description="reddit wallpaper-utility")
	arg = parser.add_argument

	arg("-d", "--database", type=str, default=b"wallpapers.db",
		help="sets the name of the database to use.")

	group = parser.add_mutually_exclusive_group(required=True)
	arg = group.add_argument

	arg("-u", "--update", action="store_true",
		help="updates the wallpaper-database using the predefined subreddits.")

	arg("-r", "--random", action="store_true",
		help="sets a random wallpaper from the database")

	arg("-e", "--export", type=str,
		help="exports a random subset of wallpapers to the given directory")

	return parser.parse_args()


def update_database(images):
	"""
	Updates the database by reading all the sources and downloading all images,
	that are not yet analyzed. The images are stored in the database, if they
	are suitable candidates for wallpapers.

	:type images: dataset.Table
	:param images: The image-table to store the data in
	"""
	image_urls = [
		url
		for source in SOURCES
		for url in load_image_urls(source)
	]

	for image_url in image_urls:
		# check if the image was already loaded
		if images.find_one(url=image_url):
			continue

		print("loading image {}".format(image_url))
		try:
			# download this image
			image_data = download_image(image_url)

			# get the size and keep this image
			width, height = Image.open(stringio.StringIO(image_data)).size
			if not is_valid_image_size(width, height):
				raise IOError("not a good image size")

			# store image in database
			images.insert(dict(
				url=image_url,
				valid=True,
				width=width,
				height=height,
				data=image_data.decode("iso-8859-1")
			))

		except IOError as err:
			print("  error loading the image: {}".format(err))
			images.insert(dict(
				url=image_url,
				valid=False
			))


def list_all_valid_images(db):
	"""
	Lists all the image-ids from the given database.

	:type db: dataset.Database
	:param db: The database as received from *dataset.connect*.
	:return: A list of valid image ids
	:rtype: list of unicode
	"""
	cursor = db.query("SELECT id FROM images WHERE valid=1")
	return [image["id"] for image in cursor]


def set_random_wallpaper(db):
	"""
	Sets a random wallpaper from the database by selecting a random (valid)
	image and using *feh* to set this as the wallpaper.

	:type db: dataset.Database
	:param db: The database to use.
	"""
	image_id = random.choice(list_all_valid_images(db))
	image = db["images"].find_one(id=image_id)

	with tempfile.NamedTemporaryFile() as fp:
		fp.write(parse_image_data(image))
		fp.flush()

		subprocess.call(["feh", "--bg-max", fp.name])


def parse_image_data(image):
	"""
	For a *image* dictionary from the database, this method will return the
	image data as a byte-string.

	:type image: dict
	:param image: a dictionary containing the *data* field
	:rtype: str
	"""
	return image["data"].encode("iso-8859-1")


def export_random_wallpapers(images, path, n=10, pattern=b"image-%d.jpg"):
	"""
	Exports *n* images from the *images* table to the given path. The filenames
	are generated using the given *pattern*.

	:param images:
	:param path:
	"""
	image_ids = list_all_valid_images(images.database)
	for idx, image_id in enumerate(random.sample(image_ids, n)):
		# get the image from the database
		image = images.find_one(id=image_id)
		image_data = parse_image_data(image)

		# write it to the file
		target = path.join(pattern % idx)
		with io.open(str(target), "wb") as fp:
			fp.write(image_data)


def main():
	args = parse_arguments()

	# connect to the (local) database
	db = dataset.connect(b"sqlite:///%s" % args.database)

	#: :type: dataset.Table
	images = db["images"]
	images.create_index(["url"])

	if args.update:
		update_database(images)

	if args.random:
		set_random_wallpaper(db)

	if args.export:
		export_path = pathlib.PosixPath(args.export)
		export_random_wallpapers(images, export_path)


if __name__ == "__main__":
	main()
